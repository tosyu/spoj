#include <stdio.h>

int shift_exponential(int a, int b, int m) {
	int i, r = 1, x = a % m;
	for (i = 1; i <= b; i <<= 1) {
		x %= m;
		if ((b & i) != 0) {
			r *= x;
			r %= m;
		}
		x *= x;
	}
	return r;
}

int main(int argc, char **argv) {
	int t, a, n;
	scanf("%d", &t);
	while (t--) {
		scanf("%d %d", &a, &n);		
		a %= 10;
		printf("%d\n", shift_exponential(a, n, 10));
	}

	return 0;
}
