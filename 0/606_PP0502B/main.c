#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
	int t, n, m;
	int *v;
	scanf("%d", &t);
	while(t--) {
		scanf("%d", &n);
		m = n;
		v = (int*) malloc(sizeof(*v) * (n + 1));
		while (n--) {
			scanf(" %d", &v[n]);
		}
		for(n = 0; n < m && printf("%d", v[n]) && (n != m -1 ? printf(" ") : 1); ++n);
		printf("\n");
	}
	return 0;
}
