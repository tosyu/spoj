#include <stdio.h>
int testPrime(int n) {
	int i;
	if (n == 1) {
		return 0;
	}
	for(i = 2; n % i != 0 && i < n ; i++);
	return i == n;
}

int main(int argc, char** argv) {
	int t, n;
	scanf("%d", &t);
	while (t--) {
		scanf("%d", &n);
		if (testPrime(n)) {
			printf("TAK\n");
		} else {
			printf("NIE\n");
		}
	}
	return 0;
}

