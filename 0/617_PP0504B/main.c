#include <stdio.h>

int main(int argc, char **argv) {
	int t, i;
	char w1[256], w2[256];
	scanf("%d", &t);
	while (t--) {
		scanf("%s %s", w1, w2);
		for (i = 0; w1[i] != '\0' && w2[i] != '\0'; i++) {
			printf("%c%c", w1[i], w2[i]);
		}
		printf("\n");
	}
	return 0;
}
