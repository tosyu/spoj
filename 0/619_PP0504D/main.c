#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
	int t, i;
	void *m = (float*) malloc(sizeof(float));
	char *c;
	scanf("%d", &t);
	while (t--) {
		scanf("%f", (float*) m);
		i = sizeof(float);
		c = (char*) m;
		while (i--) {
			printf("%x ", *(c + i) & 0xff);
		}
		printf("\n");
	}
	free(m);
	return 0;
}
