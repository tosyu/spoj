#include <stdio.h>
#include <math.h>

#define DAY 86400

int main(int argc, char **argv) {
	int t, g;
	double cmp, c, r;
	scanf("%d", &t);
	while (t--) {
		scanf("%d %lf", &g, &c);
		r = 0;
		while (g--) {
			scanf("%lf", &cmp);
			r += floor(DAY / cmp);
		}
		r = ceil(r / c);
		printf("%d\n", (int) r);
	}
	return 0;
}
