#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
	int t, n, m, r;
	scanf("%d", &t);
	while (t--) {
		scanf("%d", &n);
		r = 0;
		while (n--) {
			scanf("%d", &m);
			r += m;
		}
		printf("%d\n", r);
	}
	return 0;
}
