#include <stdio.h>

int main(int argc, char **argv) {
	int t, c, i, j;
	char word[200];

	scanf("%d", &t);
	while (t--) {
		scanf("%s", word);		
		for (i = 0; word[i] != '\0';) {
			for (j = i, c = 0; word[i] == word[j]; ++j, ++c); 
			printf("%c", word[i]);
			if (c > 2) {
				printf("%d", c);
				i += c;
			} else {
				++i;
			}
		}
		printf("\n");
	}

	return 0;
}
