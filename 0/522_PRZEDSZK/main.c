#include <stdio.h>

int gcd(int a, int b) {
	if (a == b) {
		return a;
	}

	if (a == 0) {
		return b;
	}

	if (b == 0) {
		return a;
	}

	if (~a & 1) {
		if (b & 1) {
			return gcd(a >> 1, b);
		} else {
			return gcd(a >> 1, b >> 1) << 1;
		}
	} 
	
	if (~b & 1) {
		return gcd(a, b >> 1);
	}

	if (a > b) {
		return gcd((a - b) >> 1, b);
	}
	
	return gcd((b - a) >> 1, a);
}

int lcm(int a, int b) {
	int c = a * b;
	return ((c ^ (c >> 31)) - (c >> 31)) / gcd(a, b);
}

int main (int argc, char **argv) {
	int t, a, b;
	scanf("%d", &t);
	while (t--) {
		scanf("%d %d", &a, &b);
		printf("%d\n", lcm(a, b));
	}
	return 0;
}
