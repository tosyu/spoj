#include <stdio.h>

void result(int n) {
    printf("%d %d\n", n % 100 / 10, n % 10);
}

int factorial(int n)
{

    int i, f = 1;

    for (i = 2; i <= n; i++) {
        f = f * i;
    }

    if (f <= 1) {
        return 1;
    }

    return f;
}

int main(int argc, char **argv)
{
    int t, a;

    scanf("%d", &t);
    while (t--) {
        scanf("%d", &a);
        if (a >= 10) {
            result(0);
        } else {
            result(factorial(a));
        }
    }

    return 0;
}
