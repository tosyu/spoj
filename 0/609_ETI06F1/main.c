#include <stdio.h>
#include <math.h>

int main(int argc, char **argv) {
	double r, d;
	scanf("%lf %lf", &r, &d);
	printf("%.2f\n", M_PI * pow(sqrt(pow(r,2) - pow(d/2, 2)), 2));	
	return 0;
}
