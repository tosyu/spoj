#include <stdio.h>
#include <math.h>
#include <stdlib.h>

struct Point {
    char c[10];
    int x;
    int y;
};

struct Point center = {"c", 0, 0};

int cmp(const void *a, const void *b) {
    struct Point* c = (struct Point*) a;
    struct Point* d = (struct Point*) b;
    int x1 = 0 - c->x,
        y1 = 0 - c->y,
        x2 = 0 - d->x,
        y2 = 0 - d->y,
        d1 = x1*x1+y1*y1,
        d2 = x2*x2+y2*y2;
    if (d1 > d2) {
        return 1;
    }

    if (d1 < d2) {
        return -1;
    }
    return 0;
}

int main(int argc, char **argv) {
    int t, i;
    size_t n, psize = sizeof(struct Point);

    scanf("%d", &t);
    while (t--) {
        scanf("%d\n", &i);
        n = i;
        struct Point *points = (struct Point*) malloc(n * psize);
        while (i--) {
            scanf("%s %d %d\n", &(points->c), &(points->x), &(points->y));
            if (i > 0) ++points;
        }
        i = n;
        points -= n - 1;
        qsort(points, n, psize, cmp);

        while(i--) {
            printf("%s %d %d\n", points->c, points->x, points->y);
            if (i > 0) ++points;
        }

        points -= n - 1;
        
        free(points);
        scanf("\n");
        printf("\n");
    }
    
    return 0;
}


